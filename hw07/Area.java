import java.util.Scanner;//import statement

public class Area {
	
	public static void main(String[] args){

		   String theShape = input();

		   if(theShape.equals("triangle")){
 		     Scanner x = new Scanner(System.in);
		     Scanner y = new Scanner(System.in);
		     System.out.print("Input base: ");
		     double base = x.nextDouble();
		     System.out.print("Input height:");
		     double height = y.nextDouble();
		     double triangleArea = triArea(base,height);
		     System.out.println("the are area of the triangle is" + triangleArea);
 		   }
    else if(theShape.equals("rectangle")){
         Scanner x = new Scanner(System.in);
		     Scanner y = new Scanner(System.in);
		     System.out.print("Input width: ");
		     double width = x.nextDouble();
		     System.out.print("Input length:");
		     double length = y.nextDouble();
		     double rectangleArea = recArea(width,length);
		     System.out.println("the are area of the rectangle is" + rectangleArea);
    }
      else if(theShape.equals("circle")){
         Scanner x = new Scanner(System.in);
		     System.out.print("Input radius: ");
		     double radius = x.nextDouble();
		     double circleArea = cirArea(radius);
		     System.out.println("the are area of the circle is" + circleArea);
    }
		}


   public static double triArea(double base, double height){
   double triarea = base*height*0.5;
   return triarea;
 }
   
   public static double recArea(double width, double length){
   double recarea = width*length;
   return recarea;
 }

  public static double cirArea(double radius){
   double cirarea = Math.PI * radius * radius;
   return cirarea;
 }

  public static String input(){
     String shape;
     while(true){
        System.out.println("Please input triangle, circle or rectangle");//input the value
      Scanner a = new Scanner(System.in);// scanner the scanner
      shape = a.nextLine();
      if(!shape.equals("rectangle")&&!shape.equals("triangle")&&!shape.equals("circle")){//start a loop
          System.out.println("enter the correct  shape !");
          continue;
     }else{
    	 break;
     }
     }
    return shape;
  
}
}