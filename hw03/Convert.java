// Xiaotong Huang, Feb.13.2018, HomeWork3, Convert
//
import java.util.Scanner;
//document your program
//
//
public class Convert {
  //main method required for every Java program
  public static void main (String[] args) {
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter the area of rainfall in the form xxxx.xx: ");
          double areaOfRainfall = myScanner.nextDouble();
        //enter affected area in acres
        System.out.print("Enter the depth of rainfall (in the form xx) :");
          double theDepthOfRainfall = myScanner.nextDouble();
        //enter rainfall in affected area in inch
    double volumeOfRainfallInAcreInch;
    volumeOfRainfallInAcreInch = areaOfRainfall * theDepthOfRainfall;
    //calculate the volume of rain
    double cubicMilesPerAcreInch = 2.466e-8; //convert unit
    double volumeOfRainfallInCubicMiles;
    volumeOfRainfallInCubicMiles= volumeOfRainfallInAcreInch*cubicMilesPerAcreInch;
    //volume of rainfall in cubic miles calculated
     System.out.println("Quantity of rain in cubic miles is "+ String.format("%.8f",volumeOfRainfallInCubicMiles));
    //print quantity of rain in cubic miles
    
  } //end of main method 
  
} //end of class
   