//Xiaotong Huang, Feb.13.2018, hw03, Pyramid 
//Prompt the user for the dimensions of a pyramid and returns the volume
//inside the pyramid
import java.util.Scanner;
//document your program
//
//
public class Pyramid {
  //main method required for every Java program
  public static void main (String[] args) {
     Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter the height of Pyramid in the form xx: ");
          double height = myScanner.nextDouble();
        //enter the height of the pyramid
        System.out.print("Enter the length of square under the pyramid (in the form xx) :");
          double length = myScanner.nextDouble();
       //enter the length of square under the pyramid
  
    double volume;
 volume=length*length*height/3;
   System.out.println("volume inside the pyramid " + volume);
    //print the volume inside the pyramid
  } //end of main method
} //end of class