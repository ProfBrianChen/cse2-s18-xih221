//Xiaotong Huang, Hw04, Yahtzee dice game, 2.20.2018
//
//
import java.util.Random;
import java.util.Scanner;

public class Yahtzee {
    public static void main(String[] args) { 
        // set the value of the dice
        Scanner myScanner = new Scanner(System.in);
        Random rand = new Random();
        int a; int b; int c; int d; int e;
        int upperSectionInitialTotal = 0;
        int upperSectionTotalIncludingBonus = 0;
        int lowerSectionTatol = 0;
        int grandTotal = 0;
        
        System.out.println("Type 1 to roll randomly; type other int number to enter a specific roll");
        int i = myScanner.nextInt();
        if (i == 1) {
            a = rand.nextInt(6) + 1;
            b = rand.nextInt(6) + 1;
            c = rand.nextInt(6) + 1;
            d = rand.nextInt(6) + 1;
            e = rand.nextInt(6) + 1;
            System.out.println(a + " " + b + " " + c + " " + d + " " + e);
        } else {
            System.out.println("the value of the first dice: ");
            a = myScanner.nextInt();
            System.out.println("the value of the second dice: ");
            b = myScanner.nextInt();
            System.out.println("the value of the third dice: ");
            c = myScanner.nextInt();
            System.out.println("the value of the forth dice: ");
            d = myScanner.nextInt();
            System.out.println("the value of the fifth dice: ");
            e = myScanner.nextInt();
        }
        if ((a < 1 || a > 6) || (b < 1 || b > 6) || (c < 1 || c > 6) || (d < 1 || d > 6) || (e < 1 || e > 6)) {
            System.out.println("Error");
        }
        int sum = a + b + c + d + e;
        // calculate the upper section
        for (int n = 1; n < 7; n++) {
            int num = 0;
            if (a == n) {
                num++;
            }
            if (b == n) {
                num++;
            }
            if (c == n) {
                num++;
            }
            if (d == n) {
                num++;
            }
            if (e == n) {
                num++;
            }
            //System.out.println(n + ":  +" + num * n);
            upperSectionInitialTotal = upperSectionInitialTotal + num * n;
        }
        if (upperSectionInitialTotal >= 63) {
            upperSectionTotalIncludingBonus = upperSectionInitialTotal + 35;
        } else {
            upperSectionTotalIncludingBonus = upperSectionInitialTotal;
        }
        
        // calculate the lower section
        // 3 of a kind
        if ((a == b && b == c) || (a == c && c == d) || (a == d && d == e) || (b == c && c == d) 
                ||(b == d && d == e) || (c == d && d == e)) {
            lowerSectionTatol = lowerSectionTatol + sum;
            //System.out.println("3 of a kind: + " + sum);
        }
        // 4 of a kind
        if ((a == b && b == c && c== d) || (a == c && c == d && d == e) || (b == c && c == d && d == e)) {
            lowerSectionTatol = lowerSectionTatol + sum;
            //System.out.println("4 of a kind: + " + sum);
        }
        // full house
        if ((a == b) && (c == d && d == e) || 
                (a == c) && (b == d && d == e) || 
                (a == d) && (b == c && c == e) || 
                (a == e) && (b == c && c == d) || 
                (b == c) && (a == d && d == e) || 
                (b == d) && (a == d && d == e) || 
                (b == e) && (a == c && c == d) || 
                (c == d) && (a == b && b == c) || 
                (c == e) && (a == b && b == d) || 
                (d == e) && (a == b && b == c)
                ) {
            //System.out.println("full house: + 25");
            lowerSectionTatol = lowerSectionTatol + 25;
        }
        //Small straight
        if (((a == 1 || b == 1 || c == 1 || d == 1 || e == 1) && 
                (a == 2 || b == 2 || c == 2 || d == 2 || e == 2) && 
                (a == 3 || b == 3 || c == 3 || d == 3 || e == 3) && 
                (a == 4 || b == 4 || c == 4 || d == 4 || e == 4)) || 
                ((a == 2 || b == 2 || c == 2 || d == 2 || e == 2) && 
                (a == 3 || b == 3 || c == 3 || d == 3 || e == 3) && 
                (a == 4 || b == 4 || c == 4 || d == 4 || e == 4) &&
                (a == 5 || b == 5 || c == 5 || d == 5 || e == 5)) ||
                ((a == 3 || b == 3 || c == 3 || d == 3 || e == 3) && 
                (a == 4 || b == 4 || c == 4 || d == 4 || e == 4) &&
                (a == 5 || b == 5 || c == 5 || d == 5 || e == 5) &&
                (a == 6 || b == 6 || c == 6 || d == 6 || e == 6))) {
            //System.out.println("Small straight: + 30");
            lowerSectionTatol = lowerSectionTatol + 30;
        }
        //Large straight 
        if (((a == 1 || b == 1 || c == 1 || d == 1 || e == 1) && 
                (a == 2 || b == 2 || c == 2 || d == 2 || e == 2) && 
                (a == 3 || b == 3 || c == 3 || d == 3 || e == 3) && 
                (a == 4 || b == 4 || c == 4 || d == 4 || e == 4) &&
                (a == 5 || b == 5 || c == 5 || d == 5 || e == 5)) || 
                (a == 2 || b == 2 || c == 2 || d == 2 || e == 2) && 
                (a == 3 || b == 3 || c == 3 || d == 3 || e == 3) && 
                (a == 4 || b == 4 || c == 4 || d == 4 || e == 4) &&
                (a == 5 || b == 5 || c == 5 || d == 5 || e == 5) && 
                (a == 6 || b == 6 || c == 6 || d == 6 || e == 6)) {
            //System.out.println("Large straight: + 40");
            lowerSectionTatol = lowerSectionTatol + 40;
        }
        //Yahtzee
        if (a == b && b == c && c == d && d == e) {
            //System.out.println("Yahtzee: + 50");
            lowerSectionTatol = lowerSectionTatol + 50;
        }
        //Chance
        //System.out.println("Chance: + " + sum);
        lowerSectionTatol = lowerSectionTatol + sum;
    
        //print out total
        grandTotal = upperSectionTotalIncludingBonus + lowerSectionTatol;
        System.out.println("upper section initial total = " + upperSectionInitialTotal);   
        System.out.println("upper section total including bonus = " + upperSectionTotalIncludingBonus);
        System.out.println("lower section total = " + lowerSectionTatol);
        System.out.println("grand total = " + grandTotal);
    } //main method ends
} //class ends
