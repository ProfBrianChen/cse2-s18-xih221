//Xiaotong Huang, Feb,4th,2018, hw01,welcome class
//
public class Welcome {
  
  public static void main(String[] args) {
    //Print "Welcome" to the terminal window.
   System.out.println(" ----------- ");
   System.out.println(" | WELCOME | ");
   System.out.println(" ----------- ");
   System.out.println(" ^  ^  ^  ^  ^  ^");
   System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\" );
   System.out.println("<-X--I--H--2--2--1->");                    
   System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
   System.out.println("  v  v  v  v  v  v ");
                    
  }
  
}
