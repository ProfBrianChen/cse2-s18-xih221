//April 9 2018 Xiaotong Huang 
//single dimension array
import java.util.Random;  
import java.util.Scanner;
public class CSE2Linear{
  
  //using binary search to find the grade entered by the user
  public static String rBsearch(int[] students, int gradeEntered){  //return a string 
    
    int low = 0; //smallest index 
    int high = 14; //greatest index
    int count = 0; //counts iterations
    while (high >= low) { // 
      count++; //counting how many times this while loop is run (iterations)
      int mid = Math.round((low + high)/2); //if it is even, we choose the higher number as our mid
      if (students[mid] == gradeEntered) {  //if the mid array member is equal to the number entered by the user, boolean variable foundIt is true
        return "found in the list with " + count + " iterations";
      }
        else if (students[mid] > gradeEntered) { //if the grade entered belongs to the bottom half, ignore the higher half
          high = mid - 1; 
        }
          else if (students[mid] < gradeEntered) { //if the grade entered belongs to the higher half, ignore the bottom half 
            low = mid + 1;
          }
    }   
      //in the other case, grade entered was not found
      return "not found in the list with " + count + " iterations";
  }
    
  //using linear search method to find the grade entered among the not sorted (scramble) array members
  public static String LinearSearch(int[] students, int gradeEntered){ 
    int i = 0; 
    int count = 0; // counts iteraions
  for (i = 0; i < students.length; i++) {
    count++; 
    if (students[i] == gradeEntered) { 
    return "found in the list with " + count + " iterations";
    }
  }
    //in the other case, grade entered was not found
    return "not found in the list with " + count + " iterations";
  }
    
  //scramble the sorted array randomly
  public static void Scramble(int[] students) { 
  
    Random scramble = new Random(); //declare and construct the Random instance
    int i = 0;
    for (i = 0; i < students.length; i++) { 
      //find a random number to swap with
      int target = (int) ( students.length * Math.random() );
      //swap the values
      int swap = students[target]; 
        students[target] = students[i];
        students[i] = swap;
    }
    //print out the swaped values
    for (i = 0; i < 15; i++) { 
      System.out.print(students[i] + " ");
    }
    
  }
    
  //the main method
  public static void main(String[] args) {
    //declare and construct Scanner instance 
    Scanner myScanner = new Scanner(System.in);
    int i = 0;
    //declare the array students and allocate 15 as the length of the array
    int[] students = new int[15];
    
    System.out.println("Enter 15 ascending final grades in CSE2: ");
    for (i = 0; i < 15; i++) {
      //checking whether the input is an integer or not 
      while (!myScanner.hasNextInt()) {
        System.out.println("Error");
        System.out.println("enter an integer, enter the value again: ");
        myScanner.next();
        continue;
      }
      students[i] = myScanner.nextInt();
      //checking whether the input is between 0 to 100
      while (students[i] > 100 || students[i] < 0) {
        System.out.println("enter an integer between 0 to 100, enter the value again: ");
        students[i] = myScanner.nextInt();
        continue;
      }
      //checking whether the input is greater than the last one
      if (i >= 1) {
        while (students[i] < students[i - 1]) {
          System.out.println("enter an integer greater or equal to the last one, enter the value again: ");
          students[i] = myScanner.nextInt();
          continue;
        } 
      
      }
      
    }
    //print out the final input array
    for (i = 0; i < 15; i++) {
      System.out.print(students[i] + " ");
    }
    System.out.println();
    //prompts the user to enter a grade to search for
    System.out.print("Enter the grade that you want to search for: ");
    int gradeEntered = myScanner.nextInt();
    //calling the binary search method
    System.out.println(gradeEntered + " is " + rBsearch(students, gradeEntered) );
    //printing out the scrambled grades previously entered by the user
    System.out.println("Scrambled: ");
    //calling the Scramble method 
    Scramble(students);
    System.out.println();
    //prompts the user to enter a grade to search for among the scramble grades
    System.out.println("Enter the grade that you want to search for: ");
    gradeEntered = myScanner.nextInt();
    //calling the linear search method
    System.out.println(gradeEntered + " is " + LinearSearch(students, gradeEntered) );
   
  }
}