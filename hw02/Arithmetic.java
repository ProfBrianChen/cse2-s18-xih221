//Xiaotong Huang, Feb,4th,2018, hw02, Arithmetic
//
public class Arithmetic {
  //main method required ffor every Java program
  public static void main (String[] args) {
//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per box of envelopes
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;


//calculate total cost of pants
double totalCostOfPantsWithoutTax;
totalCostOfPantsWithoutTax = numPants*pantsPrice;

//calculate total cost of sweatshirts
double totalCostOfShirtsWithoutTax;
totalCostOfShirtsWithoutTax = numShirts*shirtPrice;

//calculate total cost of belts
double totalCostOfBeltsWithoutTax;
totalCostOfBeltsWithoutTax = numBelts*beltCost;

//calculate tax of pants
double salesTaxOnPants;
salesTaxOnPants = numPants*pantsPrice*paSalesTax;

//calculate tax of shirt
double salesTaxOnShirt;
salesTaxOnShirt = numShirts*shirtPrice*paSalesTax;

//calculate tax of belt
double salesTaxOnBelt;
salesTaxOnBelt = numBelts*beltCost*paSalesTax;

//total cost of purchases before tax
double totalCostBeforeTax;
totalCostBeforeTax=totalCostOfPantsWithoutTax+totalCostOfShirtsWithoutTax+totalCostOfBeltsWithoutTax;

//total sales tax
double totalSalesTax;
totalSalesTax=salesTaxOnPants+salesTaxOnShirt+salesTaxOnBelt;

//total paid for this transaction including sales tax;
double totalCost;
totalCost=totalCostBeforeTax+totalSalesTax;
    


    
 System.out.println("total cost without tax of pants is "+ String.format("%.2f",totalCostOfPantsWithoutTax)); //print cost of pants
 System.out.println("total cost without tax of shirts is "+String.format("%.2f",totalCostOfShirtsWithoutTax)); //print cost of shirts
 System.out.println("total cost without tax of belts is "+String.format("%.2f",totalCostOfBeltsWithoutTax)); //print cost of belts
 System.out.println("Sales tax of pants is "+String.format("%.2f",salesTaxOnPants));  //print tax of pants
 System.out.println("Sales tax of shirts is "+String.format("%.2f",salesTaxOnShirt)); //print tax of shirts
 System.out.println("Sales tax of belts is "+String.format("%.2f",salesTaxOnBelt)); //print tax of belts
 System.out.println("total cost before tax is "+String.format("%.2f",totalCostBeforeTax)); //print total cost before tax
 System.out.println("total sales tax is "+String.format("%.2f",totalSalesTax)); //print total sales tax 
 System.out.println("total cost is "+String.format("%.2f",totalCost)); //print total cost

  }
}




