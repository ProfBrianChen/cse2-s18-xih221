//Xiaotong Huang, Feb.16.2018, lab04, Card Generator
//Pick a random card from 52 cards represented by number 1-52
//1-13 is diamonds 14-26 is clubs 27-39 is hearts 40-52 is spades
//
import java.util.Random;
//import random number
public class CardGenerator {
  //main method required for every Java program
  public static void main (String[] args) {
    Random rand = new Random();
    int n = rand.nextInt(52)+1;
    //choose a random number from 1-52
    String cardName = "what";//define a term to describe the card name
    if( n<14 ){
      cardName = "diamonds"; //1-13 is diamonds
    }
    else if( n>13&&n<27 ){
      cardName = "clubs";// 14-26 is clubs
    }
    else if( n>26&&n<40 ){
      cardName = "hearts";//27-39 is hearts
    }
    else if( n>39&&n<53 ){
      cardName = "spades";//40-52 is spades
    }
    
    //
    int cardNumber = 0;
    if( n>1&&n<11 ){
          cardNumber = n;
    }
    else if( n>14&&n<24 ){
      cardNumber= n-13;
    }
    else if( n>27&&n<37 ){
      cardNumber = n-26;
    }
    else if( n>40&&n<50 ){
      cardNumber = n-39;
      
    }//numbers 
    String cardNumberName = "";
if(n==1&&n==14&&n==27&&n==40){
  cardNumberName = "Ace";
}
    else if( n == 11&& n == 24 && n==37&&n==50){
  cardNumberName = "Jack";
}
    else if(n==12&&n==25&&n==38&&n==51){
      cardNumberName = "Queen";
    }
    else if(n==13&&n==26&&n==39&&n==52){
      cardNumberName = "King";
    }//ace, jack, queen, king

       System.out.println("You picked the " + cardNumber + cardNumberName +"of" +cardName);
  }
  //main method ends
}//class ends
    