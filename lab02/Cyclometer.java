//Xiaotong Huang, Feb,2nd,2018, lab2, bicycle cyclometer
//
public class Cyclometer {
  //main method required ffor every Java program
  public static void main (String[] args) {
    //our input data.
int secsTrip1=480; //number of seconds for the first trip
int secsTrip2=3220; //number of seconds for the second trip
int countsTrip1=1561; //number of rotations for the first trip
int countsTrip2=9037; //number of rotaations for the second trip
//
double wheelDiameter=27.0, //the diameter of the wheel
PI=3.14159, //whaat is pi
feetPerMile=5280, //how to transfer between feet and Mile
inchesPerFoot=12, //how to transfer between feet and inch
secondsPerMinute=60; //how to transfer between minute and second
double distanceTrip1, distanceTrip2,totalDistance; //
//print
System.out.println("Trip 1 took"+
                   (secsTrip1/secondsPerMinute)+"minutes and had"+
                   countsTrip1+"counts.");
System.out.println("Trip 2 took"+
                   (secsTrip2/secondsPerMinute)+"minutes and had"+
                   countsTrip2+"counts.");
//rin the calculations; store thhe values. Document your calculation
//here.
//
//
distanceTrip1=countsTrip1*wheelDiameter*PI;
//distance in inches
//diameter in inches times PI
distanceTrip1/=inchesPerFoot*feetPerMile; //distance in miles
distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
totalDistance=distanceTrip1+distanceTrip2;
//print out output data.
System.out.println("Trip 1 was "+distanceTrip1+" miles");
System.out.println("Trip 2 was "+distanceTrip2+" miles");
System.out.println("The totaal distance was "+totalDistance+"miles");


  }   //end of main method
} //end of 


